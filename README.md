## Description
Food delivery api

## Installation

```bash
$ npm install
```

## Running the application (need docker to be installed)

```bash
$ npm run start:docker
```

## Swagger page

http://localhost:3001/swagger