import { Injectable } from '@nestjs/common';

import { ComponentsRepository } from './components.repository';

@Injectable()
export class ComponentsService {
  constructor(private readonly componentsRepository: ComponentsRepository) {}

  async getComponents() {
    return this.componentsRepository.find();
  }
}
