import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ComponentsController } from './components.controller';
import { ComponentsRepository } from './components.repository';
import { ComponentsService } from './components.service';
import { ComponentEntity, ComponentsSchema } from './schemas/components.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ComponentEntity.name, schema: ComponentsSchema },
    ]),
  ],
  controllers: [ComponentsController],
  providers: [ComponentsService, ComponentsRepository],
})
export class ComponentsModule {}
