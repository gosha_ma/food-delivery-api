import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { ImageSchema } from 'src/common/schemas/image.schema';
import { ProductNutritionSchema } from 'src/common/schemas/product-nutrition.schema';
import { ProductParamsSchema } from 'src/common/schemas/product-params.schema';
import { ToppingEntity } from 'src/modules/topping/schemas/topping.schema';

export class ProductsSchema {
  @ApiProperty()
  _id: string;

  @Prop()
  @ApiProperty({ type: ToppingEntity })
  toppings: Types.ObjectId;

  @Prop({ required: true })
  @ApiProperty()
  title: string;

  @Prop()
  @ApiProperty()
  description: string;

  @Prop({ required: true })
  @ApiProperty()
  price: number;

  @Prop()
  @ApiProperty()
  composition: string;

  @Prop({ type: () => ProductParamsSchema, _id: false })
  @ApiProperty()
  params: ProductParamsSchema;

  @Prop({ type: () => ProductNutritionSchema, _id: false })
  @ApiProperty()
  nutrition: ProductNutritionSchema;

  @Prop()
  @ApiProperty()
  emoji: string;

  @Prop({ default: 0 })
  @ApiProperty()
  order: number;

  @Prop({ default: true })
  @ApiProperty()
  status: boolean;

  @Prop({ type: () => [ImageSchema], _id: false })
  @ApiProperty({ isArray: true, type: ImageSchema })
  images: ImageSchema[];
}
