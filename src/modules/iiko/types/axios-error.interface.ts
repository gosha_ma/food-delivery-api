import { AxiosError } from 'axios';

export interface IAxiosError {
  error: AxiosError;
}
