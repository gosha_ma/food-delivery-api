import { Controller, Get, Param } from '@nestjs/common';
import { ApiExcludeController, ApiTags } from '@nestjs/swagger';

import { IikoService } from './iiko.service';

@ApiTags('iiko')
@Controller('iiko')
@ApiExcludeController()
export class IikoController {
  constructor(private readonly iikoService: IikoService) {}

  @Get('client-data/:phone')
  async getClientData(@Param('phone') phone: string) {
    return this.iikoService.getClientData(phone);
  }
}
