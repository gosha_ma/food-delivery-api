import { Module } from '@nestjs/common';

import { IikoService } from './iiko.service';
import { IikoController } from './iiko.controller';
import { RequestManagerModule } from '../request-manager/request-manager.module';
import { RedisCacheModule } from '../redis-cache/redis-cache.module';
import { CitiesModule } from '../cities/city.module';

@Module({
  imports: [RequestManagerModule, RedisCacheModule, CitiesModule],
  controllers: [IikoController],
  providers: [IikoService],
  exports: [IikoService],
})
export class IikoModule {}
