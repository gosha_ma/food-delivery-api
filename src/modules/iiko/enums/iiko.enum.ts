export enum IikoLinks {
  GET_ACCESS_TOKEN = 'https://api-ru.iiko.services/api/1/access_token',
  CUSTOMER_INFO = 'https://api-ru.iiko.services/api/1/loyalty/iiko/get_customer',
  CREATE_ORDER = 'https://api-ru.iiko.services/api/1/deliveries/create',
}

export enum IikoRequestData {
  TYPE_PHONE = 'phone',
}

export enum DeliveryType {
  DELIVERY = 'доставка',
  PICK_UP = 'самовывоз',
  PRE_ORDER = 'предзаказ',
}

export enum IikoModifiersIds {
  SUB_PLATE = '177f14e9-2fbd-48e4-81c1-321fbdec1f41',
  PIZZA_SAUCE = 'e4f29cf1-16e5-414f-8622-5ae1c20f6964',
  SET_SOY_SAUCE = 'ec581d7a-6909-431d-ba01-520bfb5e4832',
  SET_SOY_WASABI = 'a9055fa2-dfaa-4da6-9f85-5aaa861be6be',
  SET_SOY_GINGER = '9ba03e44-b7b1-4774-9f2a-794f14ad164c',
}

export enum IikoOrganizationIds {
  KOVDOR = 'bc381c24-1036-4a26-8082-57c851e25386',
}

export enum IikoProductType {
  PRODUCT = 'Product',
}

export enum IikoOrderServiceType {
  DELIVERY_BY_COURIER = 'DeliveryByCourier',
  DELIVERY_BY_CLIENT = 'DeliveryByClient',
}
