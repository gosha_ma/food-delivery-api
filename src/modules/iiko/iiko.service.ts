import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { ICart } from '../cart/types/cart.interface';
import { CitiesService } from '../cities/city.service';
import { Cities, MurmanskDistricts } from '../cities/types/city.enum';
import { CityErrors } from '../cities/types/city.errors';
import { MurmanskPointsIds } from '../points/types/point.enum';
import { PointErrors } from '../points/types/point.errors';
import { RedisCacheService } from '../redis-cache/redis-cache.service';
import { RequestManagerService } from '../request-manager/request-manager.service';
import { IClientData } from './types/client-data.interface';
import {
  DeliveryType,
  IikoLinks,
  IikoOrderServiceType,
  IikoOrganizationIds,
  IikoRequestData,
} from './enums/iiko.enum';
import { IikoErrors } from './types/iiko.errors';
import { IOrderData } from '../cart/types/order-data.interface';
import { IPickupData } from './types/pickup-data.interface';
import { IDeliveryPoint } from './types/delivery-point.interface';
import { IikoOrder } from './types/iiko-order';

@Injectable()
export class IikoService {
  constructor(
    private readonly requestManagerService: RequestManagerService,
    private readonly configService: ConfigService,
    private readonly cacheService: RedisCacheService,
    private readonly citiesService: CitiesService,
  ) {}

  async iikoAuth(): Promise<boolean> {
    const iikoToken = this.configService.get('IIKO_TOKEN');

    const authRes = await this.requestManagerService.post(
      IikoLinks.GET_ACCESS_TOKEN,
      { apiLogin: iikoToken },
    );

    if (authRes?.error) {
      return false;
    }

    const { token: iikoApiToken } = authRes;
    await this.cacheService.setWithEncryption('iikoApiToken', iikoApiToken);

    return true;
  }

  async getClientData(phone: string): Promise<IClientData | void> {
    const clientData = await this.postIikoRequest(IikoLinks.CUSTOMER_INFO, {
      organizationId: IikoOrganizationIds.KOVDOR,
      type: IikoRequestData.TYPE_PHONE,
      Phone: phone,
    });

    if (clientData?.error) {
      throw new BadRequestException();
    }

    return clientData;
  }

  async getIikoApiToken(): Promise<string> {
    return this.cacheService.getWithEncryption('iikoApiToken');
  }

  async postIikoRequest(uri: string, body: unknown): Promise<any> {
    const iikoApiToken = await this.getIikoApiToken();

    const res = await this.requestManagerService.post(uri, body, {
      Authorization: `Bearer ${iikoApiToken}`,
    });

    if (res?.error) {
      throw new InternalServerErrorException(res.error);
    }

    return res;
  }

  async getUserWalletBalance(userPhone: string): Promise<number | never> {
    const clientData = await this.postIikoRequest(IikoLinks.CUSTOMER_INFO, {
      organizationId: IikoOrganizationIds.KOVDOR,
      type: IikoRequestData.TYPE_PHONE,
      Phone: userPhone,
    });

    if (clientData?.error) {
      throw new InternalServerErrorException(clientData.error);
    }

    return clientData?.walletBalances?.[0]?.balance?.toFixed(2);
  }

  async sendIikoOrder(
    cart: ICart,
    orderData: IOrderData,
    pickupData: IPickupData,
  ): Promise<boolean | never> {
    const orderIikoDataResponse = await this.getOrderIikoData(
      orderData,
      pickupData,
    );
    if (!orderIikoDataResponse) {
      throw new InternalServerErrorException(IikoErrors.CREATE_ORDER);
    }

    const { organizationId, orderServiceType, deliveryPoint, completeBefore } =
      orderIikoDataResponse;

    const iikoOrder: IikoOrder = {
      organizationId,
      order: {
        orderServiceType,
        phone: orderData.phone,
        comment: orderData.userNote,
        customer: {
          name: orderData.name,
        },
        items: cart,
      },
    };

    if (completeBefore) {
      iikoOrder.order.completeBefore = completeBefore;
    }
    if (deliveryPoint) {
      iikoOrder.order.deliveryPoint = deliveryPoint;
    }

    const iikoRes = await this.postIikoRequest(
      IikoLinks.CREATE_ORDER,
      iikoOrder,
    );

    if (iikoRes?.error) {
      throw new BadRequestException(IikoErrors.CREATE_ORDER);
    }

    return true;
  }

  async getOrderIikoData(
    orderData: IOrderData,
    pickupData: IPickupData,
  ): Promise<
    | {
        organizationId: string;
        orderServiceType: IikoOrderServiceType;
        deliveryPoint?: IDeliveryPoint;
        completeBefore?: string;
      }
    | undefined
  > {
    switch (orderData.deliveryType) {
      case DeliveryType.DELIVERY: {
        const organizationId = await this.getPointIikoId(
          orderData.city,
          orderData?.district,
        );
        const orderServiceType = IikoOrderServiceType.DELIVERY_BY_COURIER;
        const deliveryPoint = this.createOrderDeliveryPoint(orderData);
        return { organizationId, orderServiceType, deliveryPoint };
      }
      case DeliveryType.PICK_UP: {
        const point = await this.citiesService.getPointByAddress(
          pickupData.pickupPoint,
        );
        const organizationId = point?.iikoId;
        if (!organizationId) {
          throw new NotFoundException(PointErrors.NOT_FOUND);
        }
        const orderServiceType = IikoOrderServiceType.DELIVERY_BY_CLIENT;
        return { organizationId, orderServiceType };
      }
      case DeliveryType.PRE_ORDER: {
        const organizationId = await this.getPointIikoId(
          orderData.city,
          orderData?.district,
        );
        const orderServiceType = IikoOrderServiceType.DELIVERY_BY_CLIENT;
        const completeBefore = orderData.preOrderDate;
        return { organizationId, orderServiceType, completeBefore };
      }
    }
  }

  createOrderDeliveryPoint(orderData: IOrderData) {
    const orderDeliveryPoint: IDeliveryPoint = {
      address: {
        street: {
          name: orderData.street,
        },
        house: orderData.house,
        flat: orderData.apartment,
      },
    };

    if (orderData?.entrance) {
      orderDeliveryPoint.address.entrance = orderData.entrance;
    }
    if (orderData?.door) {
      orderDeliveryPoint.address.door = orderData.door;
    }
    if (orderData?.floor) {
      orderDeliveryPoint.address.floor = orderData.floor;
    }

    return orderDeliveryPoint;
  }

  async getPointIikoId(
    cityName: string,
    district?: string,
  ): Promise<string | never> {
    const city = await this.citiesService.findCityByName(cityName);
    if (!city) {
      throw new NotFoundException(CityErrors.CITY_NOT_FOUND);
    }

    const points = await this.citiesService.getPointsByCity(city._id);
    if (!points?.length) {
      throw new NotFoundException(PointErrors.NOT_FOUND_POINTS_FOR_CITY);
    }

    if (city?.name === Cities.MURMANSK) {
      let point: string;

      switch (district) {
        case MurmanskDistricts.PERVOMAJSKIJ: {
          point = MurmanskPointsIds.PERVOMAJSKIJ;
          break;
        }
        case MurmanskDistricts.OKTYABRSKIJ: {
          if (Math.floor(Math.random() * 2)) {
            point = MurmanskPointsIds.OKTYABRSKIJ_ZORI;
          } else {
            point = MurmanskPointsIds.OKTYABRSKIJ_SKALNAYA;
          }
          break;
        }
        case MurmanskDistricts.LENINSKIJ: {
          point = MurmanskPointsIds.LENINSKIJ;
        }
        default:
          point = MurmanskPointsIds.OKTYABRSKIJ_SKALNAYA;
      }

      return point;
    } else {
      return points?.[0]?.iikoId;
    }
  }
}
