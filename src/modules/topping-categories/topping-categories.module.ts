import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import {
  ToppingsCategoryEntity,
  ToppingsCategoriesSchema,
} from './schemas/topping-category.schema';
import { ToppingCategoriesController } from './topping-categories.controller';
import { ToppingCategoriesService } from './topping-categories.service';
import { ToppingsCategoriesRepository } from './toppingsCategories.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ToppingsCategoryEntity.name, schema: ToppingsCategoriesSchema },
    ]),
  ],
  controllers: [ToppingCategoriesController],
  providers: [ToppingCategoriesService, ToppingsCategoriesRepository],
})
export class ToppingCategoriesModule {}
