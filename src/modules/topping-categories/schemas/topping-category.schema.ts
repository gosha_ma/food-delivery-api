import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

import { ImageSchema } from 'src/common/schemas/image.schema';

export type ToppingsCategoriesDocument = ToppingsCategoryEntity & Document;

@Schema({ versionKey: false, collection: 'toppingscategories' })
export class ToppingsCategoryEntity {
  @ApiProperty()
  _id: string;

  @Prop({ required: true })
  @ApiProperty()
  title: string;

  @Prop()
  @ApiProperty()
  description: string;

  @Prop()
  @ApiProperty()
  emoji: string;

  @Prop({ type: () => [ImageSchema], _id: false })
  @ApiProperty({ isArray: true, type: ImageSchema })
  images: ImageSchema[];

  @Prop({ default: true })
  @ApiProperty()
  status: boolean;
}

export const ToppingsCategoriesSchema = SchemaFactory.createForClass(
  ToppingsCategoryEntity,
);
