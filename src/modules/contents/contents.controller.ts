import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';

import { ContentsService } from './contents.service';
import { ContentEntity } from './schemas/contents.schema';

@ApiTags('contents')
@Controller('contents')
export class ContentsController {
  constructor(private readonly contentsService: ContentsService) {}

  @Get()
  @ApiOperation({ summary: 'Get all content' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ContentEntity],
  })
  async getContents() {
    return this.contentsService.getContents();
  }
}
