import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';

import { CreateProductCategoryDto } from './dto/createProductCategory.dto';
import { ProductsCategoriesService } from './products-categories.service';
import { ProductsCategoryEntity } from './schemas/products-categories.schema';

@ApiTags('products-categories')
@Controller('products-categories')
export class ProductsCategoriesController {
  constructor(
    private readonly productsCategoriesService: ProductsCategoriesService,
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get all products categories' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ProductsCategoryEntity],
  })
  async getProductsCategories() {
    return this.productsCategoriesService.getProductsCategories();
  }

  @Get(':productCategoryId')
  @ApiOperation({ summary: 'Get products category by id' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: ProductsCategoryEntity,
  })
  @ApiParam({
    name: 'productCategoryId',
    required: true,
    description: 'Products category id',
  })
  async getProductCategoryById(
    @Param('productCategoryId') productCategoryId: string,
  ) {
    return this.productsCategoriesService.getProductCategoryById(
      productCategoryId,
    );
  }

  @Post()
  @ApiOperation({ summary: 'Create products category' })
  @ApiResponse({
    status: 201,
    description: 'Success',
    type: ProductsCategoryEntity,
  })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('images'))
  async createProductCategory(
    @UploadedFile() file: Express.Multer.File,
    @Body() createProductCategoryDto: CreateProductCategoryDto,
  ) {
    return this.productsCategoriesService.createProductCategory(
      createProductCategoryDto,
    );
  }
}
