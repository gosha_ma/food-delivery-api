import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateProductCategoryDto {
  @IsString()
  @ApiProperty()
  title: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  description?: string;

  @IsString()
  @ApiProperty()
  urlName: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  emoji?: string;

  @IsString()
  @ApiProperty()
  icon: string;

  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  @ApiProperty({ required: false })
  order?: number;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({ required: false })
  status?: boolean;
}
