import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class ModifierItemSchema {
  @Prop()
  @ApiProperty()
  amount: number;
}
