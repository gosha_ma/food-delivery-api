import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import { ProductsCategoriesService } from '../products-categories/products-categories.service';
import { ProductsCategoriesErrors } from '../products-categories/types/products-categories.errors';
import { ProductsErrors } from './enums/products.errors';
import { ProductsRepository } from './products.repository';

@Injectable()
export class ProductsService {
  constructor(
    private readonly productsRepository: ProductsRepository,
    private readonly productsCategoriesService: ProductsCategoriesService,
  ) {}

  async getProducts() {
    return this.productsRepository.findProducts();
  }

  async getPopularProducts() {
    return this.productsRepository.findPopularProducts();
  }

  async getProductById(productId: string) {
    const product = await this.productsRepository.findById(productId);
    if (!product) {
      throw new NotFoundException(ProductsErrors.NOT_FOUND);
    }

    return product;
  }

  async getProductsByCategory(categoryId: string) {
    const productCategory =
      await this.productsCategoriesService.getProductCategoryByUrlOrId(
        categoryId,
      );
    if (!productCategory) {
      throw new HttpException(
        ProductsCategoriesErrors.NOT_FOUND,
        HttpStatus.NOT_FOUND,
      );
    }

    return this.productsRepository.findProductsByCategory(productCategory._id);
  }

  async createProduct(product: any) {
    return this.productsRepository.create(product);
  }
}
