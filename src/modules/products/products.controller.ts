import { Controller, Get, Param } from '@nestjs/common';
import { ApiResponse, ApiTags, ApiOperation, ApiParam } from '@nestjs/swagger';

import { ProductsService } from './products.service';
import { ProductEntity } from './schemas/product.schema';

@ApiTags('products')
@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}

  @Get()
  @ApiOperation({ summary: 'Get all products' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ProductEntity],
  })
  async getProducts(): Promise<ProductEntity[] | null> {
    return this.productsService.getProducts();
  }

  @Get('popular')
  @ApiOperation({ summary: 'Get popular products' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ProductEntity],
  })
  async getPopularProducts() {
    return this.productsService.getPopularProducts();
  }

  @Get(':productId')
  @ApiOperation({ summary: 'Get product by id' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: ProductEntity,
  })
  @ApiParam({ name: 'productId', required: true, description: 'Product id' })
  async getProductById(@Param('productId') productId: string) {
    return this.productsService.getProductById(productId);
  }

  @Get('category/:categoryId')
  @ApiOperation({ summary: 'Get products by category' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ProductEntity],
  })
  @ApiParam({ name: 'categoryId', required: true, description: 'Category id' })
  async getProductsByCategory(@Param('categoryId') categoryId: string) {
    return this.productsService.getProductsByCategory(categoryId);
  }
}
