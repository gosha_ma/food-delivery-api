import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { EntityRepository } from 'src/database/entity.repository';
import { ProductEntity, ProductDocument } from './schemas/product.schema';

@Injectable()
export class ProductsRepository extends EntityRepository<ProductDocument> {
  constructor(
    @InjectModel(ProductEntity.name)
    private productModel: Model<ProductDocument>,
  ) {
    super(productModel);
  }

  async findProducts(): Promise<ProductEntity[] | null> {
    return this.productModel
      .find({ status: true })
      .populate('toppings')
      .populate('components')
      .sort({ order: 'desc' });
  }

  async findPopularProducts(): Promise<ProductEntity[] | null> {
    return this.productModel
      .find({ status: true, new: true })
      .populate('toppings')
      .populate('components')
      .sort({ order: 'desc' });
  }

  async findProductsByCategory(
    productCategoryId: string,
  ): Promise<ProductEntity[] | null> {
    return this.productModel
      .find({ status: true, category: productCategoryId })
      .populate('toppings')
      .populate('components')
      .sort({ order: 'desc' });
  }
}
