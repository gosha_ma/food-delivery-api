import { Injectable } from '@nestjs/common';
import { ToppingRepository } from './topping.repository';

@Injectable()
export class ToppingService {
  constructor(private readonly toppingRepository: ToppingRepository) {}

  async getToppings() {
    return this.toppingRepository.find();
  }

  async createTopping(topping: any) {
    return this.toppingRepository.create(topping);
  }
}
