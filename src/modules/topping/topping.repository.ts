import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { ToppingEntity, ToppingDocument } from './schemas/topping.schema';

@Injectable()
export class ToppingRepository {
  constructor(
    @InjectModel(ToppingEntity.name)
    private toppingModel: Model<ToppingDocument>,
  ) {}

  async find(): Promise<ToppingEntity[]> {
    return this.toppingModel.find();
  }

  async findOne(toppingId: string): Promise<ToppingEntity | null> {
    return this.toppingModel.findOne({ _id: toppingId });
  }

  async create(topping: any): Promise<ToppingEntity> {
    const newTopping = new this.toppingModel(topping);
    return newTopping.save();
  }
}
