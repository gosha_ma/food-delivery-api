import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export type ToppingDocument = ToppingEntity & Document;

@Schema({ versionKey: false, collection: 'toppings' })
export class ToppingEntity {
  @ApiProperty()
  _id: string;

  @Prop()
  @ApiProperty()
  iikoId: string;

  @Prop({ required: true })
  @ApiProperty()
  price: number;

  @Prop({ required: true })
  @ApiProperty()
  title: string;
}

export const ToppingSchema = SchemaFactory.createForClass(ToppingEntity);
