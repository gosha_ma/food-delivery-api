import { Injectable } from '@nestjs/common';

import { ComponentsItemsRepository } from './components-items.repository';
import { CreateComponentItemDto } from './dto/create-component-item.dto';

@Injectable()
export class ComponentsItemsService {
  constructor(
    private readonly componentsItemsRepository: ComponentsItemsRepository,
  ) {}

  create(createComponentItemDto: CreateComponentItemDto) {
    return this.componentsItemsRepository.create(createComponentItemDto);
  }
}
