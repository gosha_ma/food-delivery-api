import { Type } from 'class-transformer';
import { IsArray, ValidateNested } from 'class-validator';
import { CreateComponentItemDto } from './create-component-item.dto';

export class CreateManyComponentsItems {
  @IsArray()
  @ValidateNested()
  @Type(() => CreateComponentItemDto)
  readonly items: CreateComponentItemDto[];
}
