import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document, Types } from 'mongoose';

import { LocationSchema } from './location.schema';
import { ScheduleSchema } from './schedule.schema';
import { CityEntity } from 'src/modules/cities/schemas/city.schema';

export type PointDocument = PointEntity & Document;

@Schema({ versionKey: false, collection: 'points' })
export class PointEntity {
  @ApiProperty()
  _id: string;

  @Prop({ required: true })
  @ApiProperty({ type: CityEntity })
  city: Types.ObjectId;

  @Prop({ required: true })
  @ApiProperty()
  name: string;

  @Prop({ required: true })
  @ApiProperty()
  address: string;

  @Prop()
  @ApiProperty()
  iikoId: string;

  @Prop({ required: true })
  @ApiProperty()
  phone: string;

  @Prop({ type: () => ScheduleSchema, required: true, _id: false })
  @ApiProperty()
  schedule: ScheduleSchema;

  @Prop({ type: () => LocationSchema, required: true, _id: false })
  @ApiProperty()
  location: LocationSchema;

  @Prop({ default: 0 })
  @ApiProperty()
  order: number;

  @Prop({ default: true })
  @ApiProperty()
  status: boolean;
}

export const PointSchema = SchemaFactory.createForClass(PointEntity);
