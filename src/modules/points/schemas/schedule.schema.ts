import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class ScheduleSchema {
  @Prop()
  @ApiProperty()
  open: string;

  @Prop()
  @ApiProperty()
  close: string;

  @Prop()
  @ApiProperty()
  start: string;

  @Prop()
  @ApiProperty()
  end: string;

  @Prop({ default: false })
  @ApiProperty()
  closed: boolean;
}
