import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { EntityRepository } from 'src/database/entity.repository';
import { PointEntity, PointDocument } from './schemas/point.schema';

@Injectable()
export class PointsRepository extends EntityRepository<PointDocument> {
  constructor(
    @InjectModel(PointEntity.name)
    private pointsModel: Model<PointDocument>,
  ) {
    super(pointsModel);
  }

  async getPoints(): Promise<PointEntity[]> {
    return this.pointsModel.find().sort({ order: 'asc' });
  }
}
