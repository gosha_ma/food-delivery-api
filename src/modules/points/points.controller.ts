import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';

import { PointsService } from './points.service';
import { PointEntity } from './schemas/point.schema';

@ApiTags('points')
@Controller('points')
export class PointsController {
  constructor(private readonly pointsService: PointsService) {}

  @Get()
  @ApiOperation({ summary: 'Get all points' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [PointEntity],
  })
  findAll() {
    return this.pointsService.findAll();
  }
}
