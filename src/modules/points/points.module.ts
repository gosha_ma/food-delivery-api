import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { PointsService } from './points.service';
import { PointsController } from './points.controller';
import { PointEntity, PointSchema } from './schemas/point.schema';
import { PointsRepository } from './points.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: PointEntity.name, schema: PointSchema },
    ]),
  ],
  controllers: [PointsController],
  providers: [PointsService, PointsRepository],
  exports: [PointsService],
})
export class PointsModule {}
