export enum FileType {
  JPG = 'jpg',
}

export enum FileFolder {
  UPLOADS = '/uploads',
  SRC = '/public/uploads',
}
