export type IImage = {
  src: string;
  ext: string;
};
