import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiParam } from '@nestjs/swagger';

import { ComponentsCategoriesService } from './components-categories.service';
import { ComponentsCategoryEntity } from './schemas/components-categories.schema';

@ApiTags('components-categories')
@Controller('components-categories')
export class ComponentsCategoriesController {
  constructor(
    private readonly componentsCategoriesService: ComponentsCategoriesService,
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get all components categories' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ComponentsCategoryEntity],
  })
  async getComponentsCategories() {
    return this.componentsCategoriesService.getComponentsCategories();
  }

  @Get(':componentsCategoryId')
  @ApiOperation({ summary: 'Get components category by id' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: ComponentsCategoryEntity,
  })
  @ApiParam({
    name: 'componentsCategoryId',
    required: true,
    description: 'Component category id',
  })
  async getComponentsCategoryById(
    @Param('componentsCategoryId') componentsCategoryId: string,
  ) {
    return this.componentsCategoriesService.getComponentsCategoryById(
      componentsCategoryId,
    );
  }
}
