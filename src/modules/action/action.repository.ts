import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { EntityRepository } from 'src/database/entity.repository';
import { ActionDocument, ActionEntity } from './schemas/action.schema';

@Injectable()
export class ActionsRepository extends EntityRepository<ActionDocument> {
  constructor(
    @InjectModel(ActionEntity.name)
    private actionModel: Model<ActionDocument>,
  ) {
    super(actionModel);
  }

  async getActions(): Promise<ActionEntity[]> {
    return this.actionModel.find({ status: true }).sort({ order: 'desc' });
  }
}
