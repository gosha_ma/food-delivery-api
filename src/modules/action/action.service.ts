import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import { ActionsRepository } from './action.repository';
import { CreateActionDto } from './dto/createAction.dto';
import { FilesService } from '../files/files.service';
import { ActionErrors } from './types/action.errors';
import { FileErrors } from '../files/types/file.errors';
import { ActionEntity } from './schemas/action.schema';

@Injectable()
export class ActionsService {
  constructor(
    private readonly actionsRepository: ActionsRepository,
    private readonly filesService: FilesService,
  ) {}

  async getActions(): Promise<ActionEntity[] | null> {
    return await this.actionsRepository.getActions();
  }

  async getActionById(actionId: string): Promise<ActionEntity> {
    const action = await this.actionsRepository.findOne({
      _id: actionId,
      status: true,
    });
    if (!action) {
      throw new NotFoundException(ActionErrors.NOT_FOUND_ACTION);
    }

    return action;
  }

  async createAction(
    createActionDto: CreateActionDto,
    file: Express.Multer.File,
  ): Promise<ActionEntity> {
    if (!file) {
      throw new BadRequestException(FileErrors.NO_IMAGE);
    }
    const imagesResult = await this.filesService.saveImage(file);

    return this.actionsRepository.create({
      ...createActionDto,
      images: imagesResult,
    });
  }
}
