import { Request } from 'express';

import { UserEntity } from '../schemas/user.schema';

export type RequestWithUser = Request & { user: UserEntity };
