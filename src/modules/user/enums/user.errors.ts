export enum UserErrors {
  EXISTING_EMAIL = 'Пользователь с таким email уже существует',
  EXISTING_PHONE = 'Пользователь с таким телефоном уже существует',
}
