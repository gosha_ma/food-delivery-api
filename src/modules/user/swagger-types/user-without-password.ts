import { ApiProperty } from '@nestjs/swagger';

export class SwaggerUserEntity {
  @ApiProperty()
  _id: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  secondName: string;

  @ApiProperty()
  phone: string;

  @ApiProperty()
  email: string;
}
