import { BadRequestException, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

import { CreateUserDto } from './dto/create-user.dto';
import { UserEntity } from './schemas/user.schema';
import { UserErrors } from './enums/user.errors';
import { UsersRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(private readonly usersRepository: UsersRepository) {}

  async findAll(): Promise<UserEntity[]> {
    return this.usersRepository.find();
  }

  async findById(userId: string): Promise<UserEntity | null> {
    return this.usersRepository.findOne({ _id: userId });
  }

  async findByPhone(phone: string): Promise<UserEntity | null> {
    return this.usersRepository.findOne({ phone });
  }

  async findByEmail(email: string): Promise<UserEntity | null> {
    return this.usersRepository.findOne({ email });
  }

  async create(
    createUserDto: CreateUserDto,
  ): Promise<Omit<UserEntity, 'password'>> {
    const { phone, email } = createUserDto;

    const userWithSamePhone = await this.findByPhone(phone);
    if (userWithSamePhone) {
      throw new BadRequestException(UserErrors.EXISTING_PHONE);
    }

    const userWithSameEmail = await this.findByEmail(email);
    if (userWithSameEmail) {
      throw new BadRequestException(UserErrors.EXISTING_EMAIL);
    }

    const { password } = createUserDto;
    const hashPassword = await bcrypt.hash(password, 10);

    return this.usersRepository.create({
      ...createUserDto,
      password: hashPassword,
    });
  }
}
