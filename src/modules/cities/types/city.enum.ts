export enum Cities {
  MURMANSK = 'мурманск',
}

export enum MurmanskDistricts {
  PERVOMAJSKIJ = 'первомайский',
  OKTYABRSKIJ = 'октябрьский',
  LENINSKIJ = 'ленинский',
}
