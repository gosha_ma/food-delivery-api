import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';

import { CitiesService } from './city.service';
import { CityEntity } from './schemas/city.schema';

@ApiTags('cities')
@Controller('cities')
export class CitiesController {
  constructor(private readonly citiesService: CitiesService) {}

  @Get()
  @ApiOperation({ summary: 'Get all cities' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [CityEntity],
  })
  findAll() {
    return this.citiesService.findAll();
  }
}
