import { Injectable } from '@nestjs/common';
import { Types } from 'mongoose';

import { PointsService } from '../points/points.service';
import { CitiesRepository } from './city.repository';

@Injectable()
export class CitiesService {
  constructor(
    private readonly citiesRepository: CitiesRepository,
    private readonly pointsService: PointsService,
  ) {}

  async findAll() {
    return this.citiesRepository.getCities();
  }

  async findCityByName(cityName: string) {
    return this.citiesRepository.findOne({ name: cityName });
  }

  async getPointsByCity(cityId: Types.ObjectId) {
    return this.pointsService.getPointsByCity(cityId);
  }

  async getPointByAddress(name: string) {
    return this.pointsService.getPointByAddress(name);
  }
}
