import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class RequestManagerService {
  constructor(private httpService: HttpService) {}

  async get(uri: string) {
    try {
      const { data } = await firstValueFrom(this.httpService.get(uri));
      return data;
    } catch (error) {
      return { error };
    }
  }

  async post(uri: string, body: unknown, headers?: Record<string, any>) {
    try {
      const { data } = await firstValueFrom(
        this.httpService.post(uri, body, { headers }),
      );
      return data;
    } catch (error) {
      return { error };
    }
  }
}
