import { Injectable } from '@nestjs/common';

import { IikoService } from '../iiko/iiko.service';
import { IPickupData } from '../iiko/types/pickup-data.interface';
import { ICart } from './types/cart.interface';
import { IOrderData } from './types/order-data.interface';

@Injectable()
export class CartService {
  constructor(private readonly iikoService: IikoService) {}

  async sendOrder(cart: ICart, orderData: IOrderData, pickupData: IPickupData) {
    return this.iikoService.sendIikoOrder(cart, orderData, pickupData);
  }
}
