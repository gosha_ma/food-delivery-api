import { ProductNutritionSchema } from 'src/common/schemas/product-nutrition.schema';
import { ProductParamsSchema } from 'src/common/schemas/product-params.schema';

export interface IComponentItem {
  _id: string;

  title: string;

  order: number;

  iikoId: string;

  iikoGroupId: string;

  status: boolean;

  details: boolean;

  description: string;

  price: number;

  count: number;

  params: ProductParamsSchema;

  nutrition: ProductNutritionSchema;
}
