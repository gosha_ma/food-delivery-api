export interface ITopping {
  _id: string;

  iikoId: string;

  price: number;

  title: string;

  count: number;
}
