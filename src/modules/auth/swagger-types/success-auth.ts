import { ApiProperty } from '@nestjs/swagger';

export class SuccessAuth {
  @ApiProperty()
  access_token: string;
}
