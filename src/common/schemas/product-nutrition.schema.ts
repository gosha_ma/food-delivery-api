import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class ProductNutritionSchema {
  @Prop()
  @ApiProperty()
  kcal: number;

  @Prop()
  @ApiProperty()
  proteins: number;

  @Prop()
  @ApiProperty()
  carbohydrates: number;

  @Prop()
  @ApiProperty()
  fats: number;
}
