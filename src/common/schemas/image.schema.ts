import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class ImageSchema {
  @Prop()
  @ApiProperty()
  src: string;

  @Prop()
  @ApiProperty()
  width: number;

  @Prop()
  @ApiProperty()
  height: number;

  @Prop()
  @ApiProperty()
  ext: string;
}
